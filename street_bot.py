import cherrypy
import telebot
from telebot import types
import dbworker as dbw
from time import sleep
from math import radians, cos, sin, asin, sqrt


TOKEN = '650253100:AAFJ7Pc-2-S8B7zfZl5YUoRK2-GaIkEek0Q'		#StreetEda_bot

bot = telebot.TeleBot(TOKEN)

CATEGORYS = {
	'top' : ('14', '22', '15', '24'),
	'burger' : ('2', '3', '17', '24', '28'),
	'shaur' : ('1', '8', '9', '18', '24'),
	'falafel' : ('1', '2', '9', '25'),
	'hotdog' : ('8', '11', '12', '22', '26'),
	'ramen' : ('10', '24', '15', '21', '29', '30'),
	'vegan' : ('9', '2', '25'),
	'other' : ('14', '23', '20', '13', '27', '31', '32'),
	'delivery' : ('27', '28', '29', '30')
}

METRO = {
	'contract' : ('1', '3', '15', '29'),
	'kreshatik' : ('2', '29'),
	'maidan' : ('9',),
	'arsenal' : ('10',),
	'vorota' : ('13',),
	'teatr' : ('12', '14'),
	'palac' : ('24', '25'),
	'nyvki' : ('8',),
	'kpi' : ('18', '29'),
	'shulya' : ('22',),
	'univer' : ('11',),
	'obolon' : ('23',),
	'lukyan' : ('20',),
	'drughbi' : ('17',)
}


start_kb = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
close_place_btn = types.KeyboardButton("🍔 Ближайшие заведения")
obzori_btn = types.KeyboardButton("🕵 Категории")
metro_btn = types.KeyboardButton("🚇 Около метро")
start_kb.add(close_place_btn,obzori_btn, metro_btn)

menu_btn = types.KeyboardButton("⬅ Назад")

req_loc_kb = types.ReplyKeyboardMarkup(resize_keyboard=True,one_time_keyboard=True, row_width=1)
req_loc_btn = types.KeyboardButton("📍 Отправить свою локацию", request_location=True)
req_loc_kb.add(req_loc_btn, menu_btn)

metro_kb = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
centr_btn = types.KeyboardButton("💼 Центр")
rightb_btn = types.KeyboardButton("🔵 Не центр")
metro_kb.add(centr_btn, rightb_btn, menu_btn)


def format_article(name, tag, price_tag, address, descr, mark):
	if len(address) > 1:
		address = '\n'.join(address)
		address = f"\n{address}"
	else:
		address = '\n'.join(address)
	if descr:
		result = f"<b>Название: </b>{name}\n<b>Что:</b> {tag}\n<b>Цена:</b> {price_tag}\n<b>Адрес: </b>{address}\n{descr}\n<b>Оценка:</b> {mark}"
	else:
		result = f"<b>Название: </b>{name}\n<b>Что:</b> {tag}\n<b>Цена:</b> {price_tag}\n<b>Адрес: </b>{address}\n<b>Оценка:</b> {mark}"
	return result

def haversine(lat1, lon1, lat2, lon2):
    """
    Вычисляет расстояние в километрах между двумя точками, учитывая окружность Земли.
    https://en.wikipedia.org/wiki/Haversine_formula
    """

    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, (lon1, lat1, lon2, lat2))

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))
    km = 6367 * c
    return km

def find_closest(lat_user, long_user, num):
	"""
	distances: dict with distance values
	num: number of values to return
	Returns list of keys
	"""
	distances = {}
	for i in range(1,30):
		lat = dbw.get_lat(i)
		long = dbw.get_long(i)
		distances[i] = []
		for j in range(len(lat)):
			distances[i].append(haversine(lat[j], long[j], lat_user, long_user))
	d_list = []
	for i in distances.keys():
		for j in distances[i]:
			d_list.append(j)


	#new block
	min_keys = set()
	while len(min_keys) < num:
		min_distance = min(d_list)
		d_list.remove(min_distance)
		for i in distances.keys():
			for j in range(len(distances[i])):
				if distances[i][j] == min_distance:
					min_keys.add(i)
	return min_keys


	# min_list = []
	# for i in range(num):
	# 	min_list.append(min(d_list))
	# 	d_list.remove(min(d_list))
	# min_keys = set()
	# for i in distances.keys():
	# 	for j in range(len(distances[i])):
	# 		if distances[i][j] in min_list:
	# 			min_keys.add(i)
	# return min_keys


def send_place(chat_id, place_id):
	mk = types.InlineKeyboardMarkup()
	article_btn = types.InlineKeyboardButton(text='🕵 Обзор', url=dbw.get_obzor_link(place_id))
	mk.add(article_btn)

	map_link = dbw.get_map_link(place_id)
	for i in range(len(map_link)):
		addr = dbw.get_address(place_id)[i]
		mk.add(types.InlineKeyboardButton(text=f'🗺 {addr}', url=map_link[i]))
	bot.send_photo(
		chat_id = chat_id,
		photo = dbw.get_img_link(place_id),
		caption = format_article(
			dbw.get_name(place_id),
			dbw.get_tag(place_id),
			dbw.get_price_tag(place_id),
			dbw.get_address(place_id),
			dbw.get_descr(place_id),
			dbw.get_mark(place_id)),
		parse_mode = 'HTML',
		reply_markup = mk
		)
	sleep(0.5)


@bot.message_handler(commands=['start', 'help'])
def start(m):
	bot.send_message(m.chat.id, "Я бот-агрегатор обзоров от канала @streeteda. Я помогу тебе с выбором заведения 🙃", reply_markup=start_kb)
	dbw.add_user(m.chat.id, m.from_user.username)


@bot.message_handler(func = lambda m: m.text == close_place_btn.text)
def request_loc(m):
	bot.send_message(m.chat.id, "Отправь мне любою точку на карте и я пришлю тебе 5 ближайших заведения к этой точке.\n(Используй функцию отправки геопозиции или кнопку ниже)", reply_markup=req_loc_kb)


@bot.message_handler(content_types=['location'])
def handl_loc(m):
	#50 - широта(latitude)
	#30 - долгота(longitude)
	lat_user = m.location.latitude
	long_user = m.location.longitude
	ids = find_closest(lat_user, long_user, 5)
	for place_id in ids:
		send_place(m.chat.id, place_id)
		

@bot.message_handler(content_types=['photo'])
def photo_received(m):
	print(m.photo[0].file_id)

#TODO categoryes
@bot.message_handler(func = lambda m: m.text == obzori_btn.text)
def choose_category(m):
	category_kb = types.InlineKeyboardMarkup(row_width=2)
	ctop_btn = types.InlineKeyboardButton("🍒 ТОП", callback_data="category_top")
	cburger_btn = types.InlineKeyboardButton("🍔 Бургеры", callback_data="category_burger")
	cshaur_btn = types.InlineKeyboardButton("🌯 Шаурма", callback_data="category_shaur")
	cfalafel_btn = types.InlineKeyboardButton("🌮 Фалафель", callback_data="category_falafel")
	chotdog_btn = types.InlineKeyboardButton("🌭 Хот-дог", callback_data="category_hotdog")
	cramen_btn = types.InlineKeyboardButton("🍜 Рамен/нудл/лапша", callback_data="category_ramen")
	cvegan_btn = types.InlineKeyboardButton("🥗 Вегетарианское", callback_data="category_vegan")
	cother_btn = types.InlineKeyboardButton("🍕 Другое", callback_data="category_other")
	cdeliv_btn = types.InlineKeyboardButton("🛴 Доставка", callback_data="category_delivery")
	category_kb.row(ctop_btn)
	category_kb.add(cburger_btn, cshaur_btn, cfalafel_btn, chotdog_btn, cramen_btn, cvegan_btn)
	category_kb.row(cother_btn)
	category_kb.row(cdeliv_btn)
	bot.send_message(m.chat.id, "Выбери категорию:", reply_markup=category_kb)


@bot.message_handler(func = lambda m: m.text == menu_btn.text)
def menu(m):
	bot.send_message(m.chat.id, "Я бот-агрегатор обзоров от канала @streeteda. Я помогу тебе с выбором заведения 🙃", reply_markup=start_kb)


@bot.message_handler(func = lambda m: m.text == metro_btn.text)
def send_bank(m):
	bot.send_message(m.chat.id,
		"Давай уточним...\
		\n💼 Центр:\nКонтрактовая, Крещатик, Майдан Независимости,\
	Арсенальная, Золотые Ворота, Палац Спорта, Тетральная\
	\n🔵 Не центр:\nНивки, Политехническая, Шулявская, Университет,\
	Оболонь, Лукьяновская, Дружбы Народов",
		reply_markup=metro_kb)


@bot.message_handler(func = lambda m: m.text == centr_btn.text)
def send_centr(m):
	kb = types.InlineKeyboardMarkup(row_width=1)
	contract_btn = types.InlineKeyboardButton("Ⓜ Контрактовая", callback_data="metro_contract")
	kreshatik_btn = types.InlineKeyboardButton("Ⓜ Крещатик", callback_data="metro_kreshatik")
	maidan_btn = types.InlineKeyboardButton("Ⓜ Майдан Независимости", callback_data="metro_maidan")
	arsenal_btn = types.InlineKeyboardButton("Ⓜ Арсенальная", callback_data="metro_arsenal")
	vorota_btn = types.InlineKeyboardButton("Ⓜ Золотые Ворота", callback_data="metro_vorota")
	palac_btn = types.InlineKeyboardButton("Ⓜ Палац Спорта", callback_data="metro_palac")
	teatr_btn = types.InlineKeyboardButton("Ⓜ Тетральная", callback_data="metro_teatr")
	kb.add(contract_btn, kreshatik_btn, maidan_btn, arsenal_btn, vorota_btn, palac_btn)
	bot.send_message(m.chat.id, "Выбери станцию метро", reply_markup=kb)

@bot.message_handler(func = lambda m: m.text == rightb_btn.text)
def send_right_bank(m):
	kb = types.InlineKeyboardMarkup(row_width=1)
	nyvki_btn = types.InlineKeyboardButton("Ⓜ Нивки", callback_data="metro_nyvki")
	kpi_btn = types.InlineKeyboardButton("Ⓜ Политехническая", callback_data="metro_kpi")
	shulya_btn = types.InlineKeyboardButton("Ⓜ Шулявская", callback_data="metro_shulya")
	univer_btn = types.InlineKeyboardButton("Ⓜ Университет", callback_data="metro_univer")
	obolon_btn = types.InlineKeyboardButton("Ⓜ Оболонь", callback_data="metro_obolon")
	lukyan_btn = types.InlineKeyboardButton("Ⓜ Лукьяновская", callback_data="metro_lukyan")
	drughbi_btn = types.InlineKeyboardButton("Ⓜ Дружбы народов", callback_data="metro_drughbi")
	kb.add(nyvki_btn, kpi_btn, shulya_btn, univer_btn, obolon_btn, lukyan_btn, drughbi_btn)
	bot.send_message(m.chat.id, "Выбери станцию метро", reply_markup=kb)
	


@bot.callback_query_handler(func = lambda call: "category" in call.data)
def send_category(call):
	category = call.data.split("_")[1]
	for id in CATEGORYS[category]:
		send_place(call.message.chat.id, id)


@bot.callback_query_handler(func = lambda call: "metro" in call.data)
def send_metro(call):
	metro = call.data.split("_")[1]
	for id in METRO[metro]:
		send_place(call.message.chat.id, id)



# if __name__ == "__main__":
# 	bot.polling()

class WebhookServer(object):
    # index равнозначно /, т.к. отсутствию части после ip-адреса (грубо говоря)
    @cherrypy.expose
    def index(self):
        length = int(cherrypy.request.headers['content-length'])
        json_string = cherrypy.request.body.read(length).decode("utf-8")
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        sleep(1)


if __name__ == '__main__':
    cherrypy.config.update({
        'server.socket_host': '127.0.0.1',
        'server.socket_port': 7771,
        'engine.autoreload.on': False
    })
    cherrypy.quickstart(WebhookServer(), '/', {'/': {}})